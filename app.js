
const express = require ('express');
const app = express();

const mongoose = require ('mongoose');
const myURI = "mongodb+srv://admin:admin@zuittbatch243-cauyan.qpz0r65.mongodb.net/?retryWrites=true&w=majority"

//Set notifications for connection success or failure
mongoose.connect(myURI, {
    useNewUrlParser: true,
    useUnifiedTopology:true
});

app.use(express.json()); 
app.use(express.urlencoded({extended:true}));

const port = 8000;
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the cloud database"));


const usersSchema = new mongoose.Schema ({
    username: String,
    password: String
    
});

const User = mongoose.model("User", usersSchema);

app.post("/signup", (req,res)=>{
    User.findOne({username: req.body.username}, (err,result)=> {
        console.log(req.body);
        if (result !=null && result.username == req.body.username){
            // return a message to client/postman
            return res.send("Username is already taken!");
        }
        else{
            let newUser = new User({
                username:req.body.username,
                password: req.body.password
            })
        

            newUser.save((saveErr, savedTask)=>{
                if(saveErr){
                    return console.error(saveErr);
                }
                // No error found while creating the document
                else{
                    
                    return res.status(201).send("new user registered");
                }
            })
        }
    })

})

app.get('/users', (req,res)=>{
    User.find({}, (err,result)=>{

    if (err){
        return (console.log(err));
    }
    else{
        return res.status(200).json({
            data:result
        })
    }
})
})



app.listen(port, ()=>{
    console.log(`Server started on port ${port}`);
})